package modelo;

import java.util.ArrayList;

public class Disciplina{

	private String nome;
	private String codigo;
	private String creditos;
	private String disciplina;
	private ArrayList<Alunos> listaAlunosDisciplina;
	//metodos
	
	public Disciplina(){
		}
	public Disciplina(String umNome){
		nome = umNome;
		listaAlunosDisciplina = new ArrayList<Alunos>();
		}
	
	public Disciplina(String umNome, String umCodigo){
		nome = umNome;
		codigo = umCodigo;
		listaAlunosDisciplina = new ArrayList<Alunos>();
		}	
	public void setDisciplina (String umaDisciplina) {
		disciplina = umaDisciplina;
	}

	public String getDisciplina () {
		return disciplina;
	}
	
	public void setNome(String umNome){
		nome = umNome;
		}
	public void setCodigo(String umCodigo){
		codigo = umCodigo;
		}
	public void setCreditos(String umCredito){
		creditos = umCredito;
		}
	
	public String getNome(){
		return nome;
		}
	public String getCodigo(){
		return codigo;
		}
	public String getCreditos(){
		return creditos;	
	}
	
	public String adicionar (Alunos umAluno) {
		
		String mensagem = "Aluno adicionado na disciplina";
		listaAlunosDisciplina.add(umAluno);
		
		return mensagem;
	}

	public String remover (Alunos umAluno) {
		listaAlunosDisciplina.remove(umAluno);
		 return "Aluno removido da disciplina";
	}	

	public void exibirAlunosCadastrados (Disciplina umaDisciplina)
	{
		for (Alunos umAlunoDisciplina : umaDisciplina.listaAlunosDisciplina)
		{
			System.out.println ("Nome: " + umAlunoDisciplina.getNome() + " Matricula: " + umAlunoDisciplina.getMatricula());
		}

}
	}
