package modelo;

import controle.ControleAluno;
import controle.ControleDisciplina;
import java.io.*;

public class CadastroAlunoDisciplina
{
		public static void main(String[] args) throws IOException{
			
			//comandos para Input e Output do Sistema
			InputStream entradaSistema = System.in;
			InputStreamReader leitor = new InputStreamReader(entradaSistema);
			BufferedReader leitorEntrada = new BufferedReader(leitor);
			String entradaTeclado;
			char menuOpcao;
			
			ControleAluno umControle = new ControleAluno();
			ControleDisciplina umControleDisciplina = new ControleDisciplina();
			
			Alunos umEstudante = new Alunos();
			Disciplina umaMateria = new Disciplina();

			do{
				//apresentando Menu Principal
				System.out.println();
				System.out.println("===========Menu===========");
				System.out.println();
				System.out.println("1- Adicionar estudante");
				System.out.println("2- Pesquisar estudante");
				System.out.println("3- Remover estudante ");
				System.out.println("4- Adicionar disciplina ");
				System.out.println("5- Pesquisar disciplina ");
				System.out.println("6- Remover disciplina ");
				System.out.println("7- Sair");
				entradaTeclado = leitorEntrada.readLine();
				menuOpcao = entradaTeclado.charAt(0);
				
				switch(menuOpcao){
					case'1':
						
						System.out.println("Digite o nome do Aluno:");
						entradaTeclado = leitorEntrada.readLine();
						String umNome = entradaTeclado;		
						
						System.out.println("Digite a matricula do Aluno:");
						entradaTeclado = leitorEntrada.readLine();
						String umaMatricula = entradaTeclado;
						
						//adicionando aluno � lista
						Alunos umAluno = new Alunos(umNome, umaMatricula);
						String mensagem = umControle.adicionar(umAluno);
						System.out.println(mensagem);
					
					break;		
					case'2':

						System.out.println("Digite o nome do aluno a ser pesquisado:");
           				entradaTeclado = leitorEntrada.readLine();
	   					String nomeAluno = entradaTeclado;
						umEstudante = umControle.pesquisar(nomeAluno);

						if((umControle.pesquisar(nomeAluno)) != null){	
							System.out.println("Nome :" + umEstudante.getNome());
							System.out.println("Matricula:" + umEstudante.getMatricula());
						} else{
							System.out.println("Aluno n�o encontrado!");
					}
					break;
					case'3':
						System.out.println("Digite o nome do Aluno a ser removido: ");
						entradaTeclado = leitorEntrada.readLine();
						nomeAluno = entradaTeclado;
						
						umEstudante = umControle.pesquisar(nomeAluno);
						mensagem = umControle.remover(umEstudante);
						System.out.println(mensagem);
					break;
					case'4':
						System.out.println("Digite o nome da disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						String nomeDisciplina = entradaTeclado;
								
						System.out.println("Digite o codigo da Disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						String codigo = entradaTeclado;
						
						System.out.println("Digite a quantidade de creditos da Disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						String creditos = entradaTeclado;
						
						//adicionando Disciplina � lista
						Disciplina umaDisciplina = new Disciplina(nomeDisciplina, codigo);
						umaDisciplina.setCreditos(creditos);
						mensagem = umControleDisciplina.adicionar(umaDisciplina);
						System.out.println(mensagem);
					break;	
					case'5':
						System.out.println("Digite o nome da Disciplina a ser pesquisada:");
						entradaTeclado = leitorEntrada.readLine();
						nomeDisciplina = entradaTeclado;
						umaMateria = umControleDisciplina.pesquisar(nomeDisciplina);

						if((umControleDisciplina.pesquisar(nomeDisciplina)) != null){	
	           				System.out.println("Nome da Materia :" + umaMateria.getNome());
							System.out.println("Codigo:" + umaMateria.getCodigo());
							System.out.println("Creditos:" + umaMateria.getCreditos());
						} else{
							System.out.println("Materia n�o encontrada!");
						}
						
					break;
					case'6':
						System.out.println("Digite o nome da Disciplina a ser removida: ");
						entradaTeclado = leitorEntrada.readLine();
						nomeDisciplina = entradaTeclado;
						
						umaMateria = umControleDisciplina.pesquisar(nomeDisciplina);
						mensagem = umControleDisciplina.remover(umaMateria);
						System.out.println(mensagem);
					
					break;
					case'7':
						menuOpcao = '0';
					break;
					default:
						System.out.println("Opcao Incorreta");
							}
					} while(menuOpcao != '0');				
	}				 
}
