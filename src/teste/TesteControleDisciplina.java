package teste;
import modelo.Disciplina;
import static org.junit.Assert.*;

import modelo.Alunos;

import org.junit.Before;
import org.junit.Test;

import controle.ControleAluno;
import controle.ControleDisciplina;
public class TesteControleDisciplina {

	private Disciplina disciplinaTest;
	private ControleDisciplina controleTest;
	
	@Before
	public void setUp() throws Exception{
	
		disciplinaTest = new Disciplina();
		controleTest = new ControleDisciplina();
	}
	
	@Test
	public void testAdicionar() {
		disciplinaTest.setNome("OO");
		assertEquals(controleTest.adicionar(disciplinaTest), "Disciplina adicionada!");
		
	}
	
	@Test
	public void testRemover() {
		disciplinaTest.setNome("OO");
		assertEquals(controleTest.remover(disciplinaTest), "Disciplina removida!");
		
	}
	@Test
	public void testPesquisar() {
		disciplinaTest.setNome("OO");
		controleTest.adicionar(disciplinaTest);
		String umNome = disciplinaTest.getNome();
		
		assertEquals(controleTest.pesquisar(umNome), disciplinaTest);
		
	}
}
