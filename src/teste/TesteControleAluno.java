package teste;

import static org.junit.Assert.*;

import modelo.Alunos;

import org.junit.Before;
import org.junit.Test;

import controle.ControleAluno;

public class TesteControleAluno {

	private Alunos alunoTest;
	private ControleAluno controleTest;
	@Before
	public void setUp() throws Exception{
	
		alunoTest = new Alunos();
		controleTest = new ControleAluno();
	}
	
	@Test
	public void testAdicionar() {
		alunoTest.setNome("Joao");
		assertEquals(controleTest.adicionar(alunoTest), "Aluno adicionado!");
		
	}
	
	@Test
	public void testRemover() {
		alunoTest.setNome("Joao");
		assertEquals(controleTest.remover(alunoTest), "Aluno removido!");
		
	}
	@Test
	public void testPesquisar() {
		alunoTest.setNome("Joao");
		controleTest.adicionar(alunoTest);
		String umNome = alunoTest.getNome();
		
		assertEquals(controleTest.pesquisar(umNome), alunoTest);
		
	}

}
