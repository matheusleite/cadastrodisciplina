package teste;

import static org.junit.Assert.*;

import modelo.Disciplina;
import modelo.Alunos;

import org.junit.Before;
import org.junit.Test;

import controle.ControleAluno;
import controle.ControleDisciplina;

public class TesteDisciplina {

private Disciplina disciplinaTest;
private Alunos alunoTest;
private ControleAluno controleTestAluno;
private ControleDisciplina controleTestDisciplina;

	
	@Before
	public void setUp() throws Exception{
	
		disciplinaTest = new Disciplina();
		alunoTest = new Alunos();
		controleTestAluno = new ControleAluno();
		controleTestDisciplina = new ControleDisciplina();
	}
	
	@Test
	public void testNome() {
		disciplinaTest.setNome("Joao");
		assertEquals(disciplinaTest.getNome(), "Joao");

	}
	@Test
	public void testCodigo() {
		disciplinaTest.setCodigo("130011126");
		assertEquals(disciplinaTest.getCodigo(), "130011126");

	}
	@Test
	public void testCreditos() {
		disciplinaTest.setCreditos("8");
		assertEquals(disciplinaTest.getCreditos(), "8");

	}
	
	@Test
	public void testAdicionar() {
		alunoTest.setNome("Joao");
		controleTestAluno.adicionar(alunoTest);
		disciplinaTest.setNome("OO");
		controleTestDisciplina.adicionar(disciplinaTest);
	
		
		assertEquals(disciplinaTest.adicionar(alunoTest), "Aluno adicionado na disciplina");
		
	}



}
