package teste;

import modelo.Alunos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteAluno {
	
	private Alunos alunoTest;
	
	@Before
	public void setUp() throws Exception{
	
		alunoTest = new Alunos();
	}
	
	@Test
	public void testNome() {
		alunoTest.setNome("Joao");
		assertEquals(alunoTest.getNome(), "Joao");

	}
	@Test
	public void testMatricula() {
		alunoTest.setMatricula("130011126");
		assertEquals(alunoTest.getMatricula(), "130011126");

	}

}