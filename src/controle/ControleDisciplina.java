package controle;

import modelo.Alunos;
import modelo.Disciplina;
import java.util.ArrayList;
public class ControleDisciplina{

			private ArrayList<Disciplina> listaDisciplinas;
			
			public ControleDisciplina(){
				listaDisciplinas = new ArrayList<Disciplina>();
				}
			
			
			public String adicionar(Disciplina umaDisciplina){
				String mensagem = "Disciplina adicionada!";
				listaDisciplinas.add(umaDisciplina);
				return mensagem;
			}
			public String remover(Disciplina umaDisciplina){
				String mensagem = "Disciplina removida!";
				listaDisciplinas.remove(umaDisciplina);
				return mensagem;
			}
			public Disciplina pesquisar(String umNome){
				for(Disciplina umaDisciplina: listaDisciplinas ){
					if(umaDisciplina.getNome().equalsIgnoreCase(umNome)){
						return umaDisciplina;		
				}	
			}return null;
			}
			
			public void matricularAluno (Alunos umAluno, Disciplina umaDisciplina) {

			umaDisciplina.adicionar(umAluno);

			}


			public void removerMatricula (Alunos umAluno, Disciplina umaDisciplina) {
				umaDisciplina.remover(umAluno);
			}

			public void exibirDisciplinas() {

			for (Disciplina umaDisciplina : listaDisciplinas) {
				System.out.println("Disciplina: " + umaDisciplina.getDisciplina() + " Codigo: " + umaDisciplina.getCodigo());
				}
			}


		public void alunosCadastrados(Disciplina umaDisciplina) {
			umaDisciplina.exibirAlunosCadastrados(umaDisciplina);

		}

}

