package controle;

import modelo.Alunos;
import java.util.ArrayList;
public class ControleAluno{
		
			private ArrayList<Alunos> listaAlunos;
			
			public ControleAluno(){
				listaAlunos = new ArrayList<Alunos>();
			}
			
			public String adicionar(Alunos umAluno){
				String mensagem = "Aluno adicionado!";	
				listaAlunos.add(umAluno);
				return mensagem;
			
			}
			public String remover(Alunos umAluno){
				String mensagem = "Aluno removido!";
				listaAlunos.remove(umAluno);
				return mensagem;
			}
			public Alunos pesquisar(String umNome){
				for(Alunos umAluno: listaAlunos ){
					if(umAluno.getNome().equalsIgnoreCase(umNome)) {
						return umAluno;
				}	
			} return null;
			}
}


